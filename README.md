To compile:
	- Run autogen.pl (see HACKING file if you see problems while running this)
	- Run configure (with intall prefix if prefereed)
	- Run make all install

To run:
	- mpirun -np [no. of processes] -am [configuration file] -hostfile [host file] [benchmark type]

An example configuration file (enable Cheetah):
	btl = tcp,sm,self
	coll = ml,tuned,libnbc,basic
	coll_ml_verbose = 1

An example configuration file:
	compute-node-1
	compute-node-2
	compute-node-3
	compute-node-4

An example benchmark:
	- You can try MVAPICH benchmark suite (http://mvapich.cse.ohio-state.edu/benchmarks/).
			Download it on your machine, compile it (follow the README of it), and run collectives
			that are within $MVAPICH_HOME/mpi/collective directory.
